// перемещение изображения по наведению мыши
(function imgMousemove () {
  
    const img = document.querySelector('.box-x img');
  
    img.onmousemove = function(e) { // отследить наведение
  
      // подготовить к перемещению
      img.style.position = 'absolute';
      moveAt(e);
  
      // двигать изображние
      function moveAt(e) {
      let offsetX = (e.clientX / window.innerWidth * 800) - 650;
      let offsetY = (e.clientY / window.innerHeight * 800) - 650;
      img.style.left = offsetX + 'px';
      img.style.top =  offsetY + 'px';
      }

      document.onmousemove = function(e) {
        moveAt(e);
      }
    }

})()
