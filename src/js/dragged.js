// перетаскивание изображения мышью
(function imgDragged () {
  
  const img = document.querySelector('.box-drabbble img');
  
    img.onmousedown = function(e) { // отследить нажатие
  
      // подготовить к перемещению
      img.style.position = 'absolute';
      moveAt(e);
  
      // передвинуть изображение 
      function moveAt(e) {
        img.style.left = e.pageX - img.offsetWidth + 'px';
        img.style.top = e.pageY - img.offsetHeight + 'px';
      }
    
      // перемещать по экрану
      document.onmousemove = function(e) {
        moveAt(e);
      }
    
      // отследить окончание переноса
      img.onmouseup = function() {
        document.onmousemove = null;
        img.onmouseup = null;
      }
    }

})()

