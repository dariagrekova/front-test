const header = document.querySelector('.header');
const main = document.querySelector('main')
window.addEventListener('scroll', () => {
  const wrapper = document.querySelector('.wrapper');
  if (window.scrollY > 1) {
    header.classList.add('fixed');
    wrapper.style.marginTop = '100px'

  } else {
    header.classList.remove('fixed');
    wrapper.style.marginTop = ''
  }

})

